
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.*;

public class Users {
    private String username;
    private String password;
    private String name;
    private String bio;
    //private Drawable imageView;
    public Socket socket;
    private int score;
    // public ArrayList<Users> Friends;
    public Map<String,ArrayList<message>> Friends_chat;
    public ArrayList<game> games;
    private DataOutputStream dos;
    private DataInputStream dis;
    private boolean xo_game =false;
    private boolean first_turn_in_xo=false;
    private boolean guess_game = false;


//    public Users(String username, String password, String name, String bio, Drawable imageView) {
//        this.username = username;
//        this.password = password;
//        this.name = name;
//        this.bio = bio;
//        this.imageView = imageView;
//    }

    //    public Users(String username, String password, Drawable imageView) throws IOException {
//        this.imageView=imageView;
//        this.username=username;
//        this.password=password;
//        this.bio=bio;
//        this.name=name;
//    }
    public Users(String username, String password, String name , String bio) throws IOException {
        this.username=username;
        this.password=password;
        this.bio=bio;
        this.name=name;
        this.Friends_chat=new HashMap<>();
//        dis=new DataInputStream(socket.getInputStream());
//        dos=new DataOutputStream(socket.getOutputStream());
    }

    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public boolean isGuess_game() {
        return guess_game;
    }

    public void setGuess_game(boolean guess_game) {
        this.guess_game = guess_game;
    }

    public  String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getBio() {
        return bio;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public boolean isXo_game() { return xo_game; }

    public boolean isFirst_turn_in_xo() {
        return first_turn_in_xo;
    }

    public void setFirst_turn_in_xo(boolean first_turn_in_xo) {
        this.first_turn_in_xo = first_turn_in_xo;
    }

    public void setXo_game(boolean xo_game) { this.xo_game = xo_game; }
    //    public Drawable getImageView() {
//        return imageView;
//    }
public ArrayList<chats_details> makeList_details(){
        ArrayList<chats_details> temp =new ArrayList<>();
       for(String key : Friends_chat.keySet()){
          ArrayList<message> tempArray=Friends_chat.get(key);
          if(tempArray.size()==0){
              chats_details empty_chat=new chats_details("empty","nothing",key);
              temp.add(empty_chat);
          }
          else {
              message lastmeesage = tempArray.get(tempArray.size() - 1);
              chats_details chats_details = new chats_details(lastmeesage.text, lastmeesage.date, lastmeesage.sender);
              temp.add(chats_details);
              return temp;
          }
       }
    return new ArrayList<>();
    }
}
class message{
   public String text;
   public String sender;
   public String date;
    message(String text,String sender,String date){
        this.date=date;
        this.sender=sender;
        this.text=text;
    }

}

class chats_details{
    private String lastmessage;
    private  String senttime;
    private String contact;
    public chats_details(String lastmessage, String senttime, String contact){
        this.contact=contact;
        this.senttime=senttime;
        this.lastmessage=lastmessage;
    }

}
class game{
    String room_name;
  public boolean draw;
  public String winner;
  public String loser;
   String namegame;
  public game(String namegame,String room_name){
       this.namegame=namegame;
       this.room_name=room_name;
   }

}