import sun.plugin2.message.Message;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Server {
    DataInputStream dis;
    DataOutputStream dos;
  //  public static   Datacenter  datacenter=new Datacenter();

    public static Map<String,ClientHandler> users_DATA=new HashMap<>();
    public static Map<String,Users> stored_data =new HashMap<>();


    public static String path ="C:\\Users\\sama laptop\\Desktop";
    public static File file = new File(path+"\\file-datacenter");


    public static void main(String[] args) throws Exception {

        boolean isCreated = file.mkdir();


        File data = new File(path+"\\file-datacenter\\all-usernames.txt");
        data.createNewFile();
        Scanner folder_scanner = new Scanner(data);
        while (folder_scanner.hasNext()){

            String folder = folder_scanner.next();
            File user_pass = new File(path+"\\file-datacenter\\"+folder+"\\user-pass.txt");
            Scanner user_pass_scanner = new Scanner(user_pass);
            String username_user = user_pass_scanner.nextLine();
            String password_user = user_pass_scanner.nextLine();




            File name = new File(path+"\\file-datacenter\\"+folder+"\\name.txt");
            Scanner name_scanner = new Scanner(name);
            String name_user = name_scanner.nextLine();




            File bio = new File(path+"\\file-datacenter\\"+folder+"\\bio.txt");
            Scanner bio_scanner = new Scanner(bio);
            String bio_user = bio_scanner.nextLine();

            Users u1 = new Users(username_user,password_user,name_user,bio_user);
            stored_data.put(username_user,u1);


            File  friends_name = new File(path+"\\file-datacenter\\"+folder+"\\friend.txt");
            friends_name.createNewFile();
            Scanner names = new Scanner(friends_name);

            while (names.hasNext()){
                ArrayList<message>chats_array = new ArrayList<>();
                String friend_name = names.nextLine();
                File chats = new File(path+"\\file-datacenter\\"+folder+"\\friend\\"+friend_name+".txt");
                chats.createNewFile();
                Scanner chat_scanner = new Scanner(chats);

                while (chat_scanner.hasNext()){
                    String text = chat_scanner.nextLine();
                    String sender=chat_scanner.nextLine();
                    String date=chat_scanner.nextLine();
                    message m1 = new message(text,sender,date);
                    chats_array.add(m1);

                }

                u1.Friends_chat.put(friend_name,chats_array);

            }




        }




        ServerSocket serverSocket=new ServerSocket(3000);
        System.out.println("start");

        while (true){

            Socket socket=serverSocket.accept();
            login_signup login_signup=new login_signup(socket);
            ClientHandler temp = new ClientHandler( socket );
            ( new Thread( temp ) ).start();
        }
    }



}
class login_signup {
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    login_signup(Socket socket) throws IOException {
        this.socket = socket;
        this.dis = new DataInputStream(socket.getInputStream());
        this.dos = new DataOutputStream(socket.getOutputStream());
    }
}