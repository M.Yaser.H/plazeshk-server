//import com.sun.tools.javac.Main;

import com.sun.javafx.font.coretext.CTFactory;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ClientHandler implements Runnable {
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private ObjectOutputStream objo;
    private String input;
    private Users user;

    ClientHandler(Socket socket) throws Exception {
        this.socket = socket;
        this.dis = new DataInputStream(socket.getInputStream());
      //  this.objo=new ObjectOutputStream(socket.getOutputStream()) ;
        this.dos = new DataOutputStream(socket.getOutputStream());
        this.user = new Users("","","","");
    }

    public void setUser(Users user) {
        this.user = user;

    }

    String username_input;
    String password_input;
    String name;
    String bio;

    @Override
    public void run() {
        //ThreadGroup threadGroup=new ThreadGroup()
        System.out.println("someone connected");
        try {
            while (true) {
                boolean done=true;
                login_signup(dos, dis,done);
                Main_process();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Users getUser() {
        return user;
    }
    public void sign_up_prcess(){
        try {
            input=dis.readUTF();
            username_input=input;
            input=dis.readUTF();
            password_input=input;
            input=dis.readUTF();
            name=input;
            input=dis.readUTF();
            bio=input;
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;

    }
    public  boolean check_input_signup(String username){
        if(Server.stored_data.containsKey(username_input)){
            return false;
        }
//        else if(pass.length()<5){
//            return false;
//        }
        else
            return true;
    }
    public  void Main_process(){
        boolean back=false;
        boolean main_process=true;
        System.out.println("main process start");
        //for first back
//        try {
//            input=dis.readUTF();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try {
            while (main_process){
                input=dis.readUTF();
                System.out.println("input from client main :"+ input);
                if(input.equals("chat")){
                    back=false;
                    while (!back){
                        input=dis.readUTF();

                        if (input.equals("back")){
                            back=true;
                        }else if(input.equals("pvchat")){

                            pvchat();
                        }
                        else  {
                            chat_process(input);
                        }

                    }
                }
                else if(input.equals("game")){
                    back=false;
                    while (!back) {
                        input=dis.readUTF();
                        switch (input) {
                            case "back":
                                back = true;
                                break;
                            case "xo":
                                String javab = xo_game_process("xo");
                                if (javab.equals("failed")) {
                                    back = true;
                                }
                                break;
                            case "guessgame":
                                guess_game();
//<<<<<<< HEAD
//                                back=true;
//
//=======
                                break;
//>>>>>>> 7402d6307c92169ff58153e8261c319016d372b5
                        }

                    } }
                else if(input.equals("people")){
                    back=false;
                    while (!back){
                        System.out.println("wait for do sth in people fragment or backk");
                        input=dis.readUTF();
                        System.out.println("input in people:"+ input);
                        if(input.equals("back")) {
                            back = true;
                        }else if(input.equals("pvchat")){
                            pvchat();
                        }
                        else {
                            people_process(input);
                        }
                    }
                }
                else if(input.equals("profile")){

                    back=false;
                    while (!back){
                        input=dis.readUTF();
                        if(input.equals("back")){
                            back=true;
                        }else if (input.equals("getdata")){
                            profile_process();
                        }else {
                            change_profile();
                        }

                    }
                }else if(input.equals("settings")){
                    back=false;

                    while (!back){
                        input=dis.readUTF();
                        if(input.equals("back")){
                            back=true;
                        }else  {

                            System.out.println("Setting process start");
                            if(input.equals("logout")){
                                System.out.println("logout start");
                                back=true;
                                main_process= false;
                                logout();
                            }
                            else  if (input.equals("about")){
                                System.out.println("about");
                                boolean repeat = false;
                                while (!repeat){
                                    input=dis.readUTF();
                                    if (input.equals("back")){
                                        repeat = true;
                                        back =true;

                                    }



                                }


                            }
                        }
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pvchat() {
        System.out.println("pvchat process start");
        try {
          String friend_username=  dis.readUTF();
          System.out.println(friend_username);
          dos.writeUTF(friend_username);
          int num_chat=getUser().Friends_chat.get(friend_username).size();
          dos.writeInt(num_chat);
          for (int i=0; i<num_chat; i++){
               message message= getUser().Friends_chat.get(friend_username).get(i);
               dos.writeUTF(message.text);
               dos.writeUTF(message.sender);
               dos.writeUTF(message.date);
            }


boolean work=true;

                while (work) {
                    input=dis.readUTF();
                    System.out.println("input in  client:"+input);
                    if (input.equals("sendtext")) {
                        System.out.println(input);
                        String text = dis.readUTF();
                        String time = dis.readUTF();
                        message message = new message(text, getUser().getUsername(), time);
                        Server.stored_data.get(getUser().getUsername()).Friends_chat.get(friend_username).add(message);
                        //   getUser().Friends_chat.get(friend_username).add(message);
                        System.out.println(text + " " + time);
                        Server.stored_data.get(friend_username).Friends_chat.get(getUser().getUsername()).add(message);

                        System.out.println("curruser" + getUser().Friends_chat.toString());
                        System.out.println("friend" + Server.stored_data.get(friend_username).Friends_chat.get(getUser().getUsername()).toString());
                        dos.writeUTF("sent");


                        File my_friend_list = new File(Server.path + "\\file-datacenter\\" + getUser().getUsername() + "\\friend\\" + friend_username + ".txt");
                        FileWriter fw1 = new FileWriter(my_friend_list, true);
                        fw1.write(message.text + "\n");
                        fw1.write(message.sender + "\n");
                        fw1.write(message.date + "\n");
                        fw1.close();


                        File friend_list_for_friend = new File(Server.path + "\\file-datacenter\\" + friend_username + "\\friend\\" + getUser().getUsername() + ".txt");
                        FileWriter fw2 = new FileWriter(friend_list_for_friend, true);
                        fw2.write(message.text + "\n");
                        fw2.write(message.sender + "\n");
                        fw2.write(message.date + "\n");
                        fw2.close();

                        dis.readUTF();
                    } else if (input.equals("exit")) {
                        work=false;
                    }
else if(input.equals("back")){
    work=false;
                    }
                }






        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logout(){
      Server.users_DATA.remove(getUser().getUsername());
      setUser(null);

    }


    private void profile_process() {
        System.out.println("profile process start");
        try {
            dos.writeUTF(getUser().getUsername());
            dos.writeUTF(getUser().getName());
            dos.writeUTF(getUser().getBio());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void change_profile() throws IOException{
        System.out.println("change profile started");
        String new_name="";
        String new_bio="";
        try {
            dos.writeUTF(getUser().getName());
            dos.writeUTF(getUser().getBio());
             new_name = dis.readUTF();
             new_bio = dis.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(new_name);
        System.out.println(new_bio);
        getUser().setName(new_name);
        getUser().setBio(new_bio);

        File file1 = new File(Server.path+"\\file-datacenter\\"+getUser().getUsername()+"\\name.txt");
        FileWriter fw1 = new FileWriter(file1);
        fw1.write(new_name);
        fw1.close();


        File file2 = new File(Server.path+"\\file-datacenter\\"+getUser().getUsername()+"\\bio.txt");
        FileWriter fw2 = new FileWriter(file2);
        fw2.write(new_bio);
        fw2.close();


    }

    private void people_process(String input) throws IOException {
        String friend_username;
        System.out.println("people process start");
        if(input.equals("addfriend")) {
            try {
                friend_username = dis.readUTF();
              if(  Server.stored_data.containsKey(friend_username)){
                      dos.writeUTF("done");
                      getUser().Friends_chat.put(friend_username,new ArrayList<>());
                  Server.stored_data.get(user.getUsername()).Friends_chat.put(friend_username,new ArrayList<>());
                  Server.stored_data.get(friend_username).Friends_chat.put(getUser().getUsername(),new ArrayList<>());
                  System.out.println(user.Friends_chat.keySet().toString());



                  File my_friendfolder = new File(Server.path+"\\file-datacenter\\"+getUser().getUsername()+"\\friend");
                  my_friendfolder.mkdir();
                  File friend_list = new File(Server.path+"\\file-datacenter\\"+getUser().getUsername()+"\\friend\\"+friend_username+".txt");
                  friend_list.createNewFile();



                  File my_friend_list = new File(Server.path+"\\file-datacenter\\"+getUser().getUsername()+"\\friend.txt");
                  my_friend_list.createNewFile();
                  FileWriter fw = new FileWriter(my_friend_list,true);
                  fw.write(friend_username+" ");
                  fw.close();



                  File friend_friendfolder = new File(Server.path+"\\file-datacenter\\"+friend_username+"\\friend");
                  friend_friendfolder.mkdir();
                  File friend_list_for_friend = new File(Server.path+"\\file-datacenter\\"+friend_username+"\\friend\\"+getUser().getUsername()+".txt");
                  friend_list_for_friend.createNewFile();



                  File friend_friend_list = new File(Server.path+"\\file-datacenter\\"+friend_username+"\\friend.txt");
                  FileWriter fw1 = new FileWriter(friend_friend_list,true);
                  fw1.write(getUser().getUsername() +" ");
                  fw1.close();




              }
              else {
                  dos.writeUTF("dont");
              }
                System.out.println(friend_username);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (input.equals("getList")) {
            dos.writeInt(getUser().Friends_chat.size());
            System.out.println(getUser().Friends_chat.size());
            for (String s :getUser().Friends_chat.keySet()){
                dos.writeUTF(s);
            }
           //dos.writeUTF("done");
            System.out.println("list friend sent");



    }

    }


    private void guess_game() {
        try {

//            System.out.println("waiting for partner in guess game");
//            dos.writeUTF("no preson");
//            Server.users_DATA.get(getUser().getUsername()).getUser().setXo_game(true);
//            getUser().setGuess_game(true);
//            String side_2_username = dis.readUTF();
//            System.out.println(side_2_username);
//            if(Server.users_DATA.containsKey(side_2_username)){
//                 System.out.println(Server.users_DATA.get(side_2_username).getUser().isGuess_game());
//            }
//            ClientHandler side_1_ch = Server.users_DATA.get(getUser().getUsername());
//            ClientHandler side_2_ch = Server.users_DATA.get(side_2_username);
//
//            if (!Server.users_DATA.containsKey(side_2_username)) {
//                dos.writeUTF("vojod nadarad");
//
//            } else if (Server.users_DATA.get(side_2_username).getUser().isGuess_game()) {
//                    side_2_ch.dos.writeUTF("guess");
//                side_1_ch.dos.writeUTF("go");
//                    tell_word(side_1_ch,side_2_ch);
//                    harf_bego(side_2_ch,side_1_ch);
//            } else
//                {
//                    dos.writeUTF("not ok");
//                }
            System.out.println("guess_game process start");
            dos.writeUTF("noperson");
            Server.users_DATA.get(getUser().getUsername()).getUser().setXo_game(true);
            getUser().setGuess_game(true);
//<<<<<<< HEAD
//            System.out.println(getUser().isGuess_game());
//            String side_2_username = dis.readUTF();
//            //System.out.println(side_2_username);
//            if(Server.users_DATA.containsKey(side_2_username)){
//                 System.out.println(Server.users_DATA.get(side_2_username).getUser().isGuess_game());}
//            ClientHandler side_1_ch = Server.users_DATA.get(getUser().getUsername());
//            ClientHandler side_2_ch = Server.users_DATA.get(side_2_username);
//
//
//
//            if (!Server.users_DATA.containsKey(side_2_username)) {
//                dos.writeUTF("vojod nadarad");
//
//            } else if (Server.users_DATA.get(side_2_username).getUser().isGuess_game()) {
//
//                    side_2_ch.dos.writeUTF("go");
//                    System.out.println("sent");
//                    side_1_ch.dos.writeUTF("guess");
//                    System.out.println("sent");
//                    loghat_bego(side_2_ch,side_1_ch);
//                   // harf_bego(side_1_ch,side_2_ch);
//            } else {
//                dos.writeUTF("not ok");
//=======
            boolean wait=true;
            while (wait) {
                String input=dis.readUTF();
                if(input.equals("person2")){
                    System.out.println(getUser().getUsername() + ": person2 start");
                    String side1 = "";
                    side1 = dis.readUTF();
                    ClientHandler side1_handler = Server.users_DATA.get(side1);
                    String word = dis.readUTF();
                    System.out.println("get:" + word);
                    int tedad_ragham = word.length();
                    int count_correct=tedad_ragham;
                    boolean onerepeat = false;
                    for (int i = 0; i < 2 * tedad_ragham; i++) {
                        if(count_correct!=0) {
                            String javab_hadsi = dis.readUTF();
                            System.out.println(javab_hadsi);
                            //    System.out.println("harf input :"+javab_hadsi);
                            for (int j = 0; j < tedad_ragham; j++) {
                                if (word.charAt(j) == javab_hadsi.charAt(0)) {
                                    count_correct--;
                                    System.out.println("correct");
                                    side1_handler.dos.writeUTF("can");
                                    dos.writeUTF("ok");
                                    dos.writeUTF(javab_hadsi);
                                    dos.writeInt(j);
                                    onerepeat = true;
                                }
                            }
                            if (!onerepeat) {
                                side1_handler.dos.writeUTF("cant");
                                System.out.println("not correct");
                                dos.writeUTF("wrong");
                            }
                            onerepeat = false;
                        }
                        else{
                            side1_handler.dos.writeUTF("endround1");
                            dos.writeUTF("endround1");
                            side1_handler.dos.writeUTF(getUser().getUsername());
                            String word2=dis.readUTF();
                            side1_handler.dos.writeUTF(word2);
                            String res=  dis.readUTF();
                            System.out.println(res);
                            break;
                        }
                    }
                    dos.writeUTF("loseround1");
                    side1_handler.dos.writeUTF("loseround1");
                    side1_handler.dos.writeUTF(getUser().getUsername());
                    String word2=dis.readUTF();
                    side1_handler.dos.writeUTF(word2);
                    String res=  dis.readUTF();
                    System.out.println(res);
                    wait = false;
                }
                else {
                    String person2 = input;
                    System.out.println(person2);
                    if (Server.users_DATA.containsKey(person2)) {
                        System.out.println("state person 2 =" + Server.users_DATA.
                                get(person2).getUser().isGuess_game());
                        if (Server.users_DATA.get(person2).getUser().isGuess_game()) {
                            dos.writeUTF("tell");
                            Server.users_DATA.get(person2).dos.writeUTF("hads");
                            Server.users_DATA.get(person2).dos.writeUTF(getUser().getUsername());
                            String result = dis.readUTF();
                            if (result.equals("person1")) {
                                System.out.println("person1 start");
                                String word = dis.readUTF();
                                ClientHandler side2_handler = Server.users_DATA.get(person2);
                                side2_handler.dos.writeUTF(word);
                              String res=  dis.readUTF();
                                System.out.println(res);
                                if(res.equals("round2")){
                                    System.out.println(getUser().getUsername() + ": person1 part 2 start");
                                    String side2 = "";
                                    side2 = dis.readUTF();
                                    ClientHandler round_2_side1 = Server.users_DATA.get(side2);
                                    String word2 = dis.readUTF();
                                    System.out.println("get:" + word2);
                                    int tedad_ragham = word2.length();
                                    int count_correct=tedad_ragham;
                                    boolean onerepeat = false;
                                    for (int i = 0; i < 2 * tedad_ragham; i++) {
                                        if(count_correct!=0) {
                                            String javab_hadsi = dis.readUTF();
                                            System.out.println(javab_hadsi);
                                            //    System.out.println("harf input :"+javab_hadsi);
                                            for (int j = 0; j < tedad_ragham; j++) {
                                                if (word2.charAt(j) == javab_hadsi.charAt(0)) {
                                                    count_correct--;
                                                    System.out.println("correct");
                                                    round_2_side1.dos.writeUTF("can");
                                                    dos.writeUTF("ok");
                                                    dos.writeUTF(javab_hadsi);
                                                    dos.writeInt(j);
                                                    onerepeat = true;
                                                }
                                            }
                                            if (!onerepeat) {
                                                round_2_side1.dos.writeUTF("cant");
                                                System.out.println("not correct");
                                                dos.writeUTF("wrong");
                                            }
                                            onerepeat = false;
                                        }
                                        else{
                                            round_2_side1.dos.writeUTF("end");

                                            dos.writeUTF("end");
                                            break;
                                        }
                                    }
                                    dos.writeUTF("lose");
                                    round_2_side1.dos.writeUTF("lose");
                                    break;






                                }
                                wait = false;
                            }
                            //  start_game_hads(side_1_ch, side_2_ch);

                        } else {
                            dos.writeUTF("not ok");
                        }
                    } else {
                        dos.writeUTF("wrong");
                            }
//>>>>>>> 7402d6307c92169ff58153e8261c319016d372b5
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void side_1(String person2) {

    }

    private void side_2 () {

//        try {
//            System.out.println("S");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }


    private void start_game_hads(ClientHandler side_1 ,ClientHandler side_2 )throws IOException
        {
            System.out.println("game start");
            boolean onerepeat=false;

        }



    private void xo_game(ClientHandler side_1 , ClientHandler side_2){
        System.out.println("xo game started");
        boolean turn =true;
        int num =0 ;
        try {
            side_1.dos.writeUTF("start");
            side_2.dos.writeUTF("wait");
            System.out.println("sent");

//            String mode1 = side_1.dis.readUTF();
//            String mode2 = side_2.dis.readUTF();
//

//            if (mode1.equals(mode2))
//                System.out.println("mode :"+mode1);


        } catch (IOException e) {
            e.printStackTrace();
        }

            int i_1=0;
            int j_1=0;
            if (turn) {
                while (num <9) {

                    try {

                        //String ii = side_1.dis.readUTF();
                        //String jj = side_1.dis.readUTF();
                        System.out.println("waiting for reading");
                        i_1 = side_1.dis.readInt();
                        j_1 = side_1.dis.readInt();
                        System.out.println("read");
                        System.out.println(i_1 + "," + j_1);

                        //dos.writeUTF("x");
                        side_2.dos.writeInt(i_1);
                        side_2.dos.writeInt(j_1);

//
                        int i_2 = side_2.dis.readInt();
                        int j_2 = side_2.dis.readInt();


                        side_1.dos.writeInt(i_2);
                        side_1.dos.writeInt(j_2);

                        System.out.println(i_2 + "," + j_2);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    turn = !turn;
                }

            } else {

                try {



//                    side_2.dos.writeInt(i_1);
//                    side_2.dos.writeInt(j_1);

                    int i_2 = side_2.dis.readInt();
                    int j_2 = side_2.dis.readInt();

                    side_1.dos.writeInt(i_2);
                    side_1.dos.writeInt(j_2);



                } catch (IOException e) {
                    e.printStackTrace();
                }
                turn = !turn;
            }

            num++;
//        }
    }


    private void game_process(String input) throws IOException {


    }

    private String  xo_game_process(String input) throws IOException {

        System.out.println("xo game process start");


//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    String javab = dis.readUTF();
//                    if (javab.equals("xo")){
//                        if (getUser().isXo_game()){
//                                dos.writeUTF("start");
//                        }
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });


        try {
            if (input.equals("xo")){
                getUser().setXo_game(true);
                getUser().setFirst_turn_in_xo(true);
                String side_2_username = dis.readUTF();
                String side_1_username = getUser().getUsername();
                //System.out.println(side_1_username);
                //System.out.println(side_2_username);
                ClientHandler side_2_client = Server.users_DATA.get(side_2_username);

                side_2_client.getUser().setXo_game(true);
                // System.out.println(side_2_client.password_input);
                // System.out.println(side_2_client.name);
                // System.out.println(side_2_client.bio);
                // System.out.println(side_2_client.getUser().isXo_game());
                // System.out.println(getUser().isXo_game());


                if (side_2_client.getUser().isXo_game() == true){
                    dos.writeUTF("start");
                    side_2_client.dos.writeUTF("start");
                    xo_game(this,side_2_client);


                }else   {
                    dos.writeUTF("not ok");
                }








            }

        }


        catch (Exception e){
            System.out.println("xo game process failed");
            return "failed";
        }

        return " ";

    }

    private void chat_process(String input) throws IOException {

        System.out.println("chat process start");
        if(input.equals("getList")){
            dos.writeInt(getUser().Friends_chat.size());
            for (String s :getUser().Friends_chat.keySet()) {
                String name = s;
                System.out.print(name);
                dos.writeUTF(name);
                if(getUser().Friends_chat.get(s).isEmpty()){
                    dos.writeUTF("empty");
                    dos.writeUTF("empty");

                }
                else {
                    String lasttext = getUser().Friends_chat.get(s).get(0).text;
                    dos.writeUTF(lasttext);
                    String time = getUser().Friends_chat.get(s).get(0).date;
                    dos.writeUTF(time);
                }

            }
            System.out.println("chats sent");
        }
    }

    public void login_signup(DataOutputStream dos, DataInputStream dis, boolean done) throws Exception {

        while (done) {
            input = dis.readUTF();
            if (input.equals("login")) {

                boolean loginchekc=true;
                while (loginchekc){
                System.out.println("login process start");
                input = dis.readUTF();
                username_input = input;
                input = dis.readUTF();
                password_input = input;
                System.out.println("login check , username : "+ username_input +" pass : " + password_input);

                if (!Server.users_DATA.containsKey(username_input) ) {
                    if (!Server.stored_data.containsKey(username_input))
                    {
                        System.out.println("wrong");
                        dos.writeUTF("wrong");
                    }
                    else if (Server.stored_data.containsKey(username_input) && Server.stored_data.get(username_input).getPassword().equals(password_input))
                    {
                        System.out.println("login done");
                        dos.writeUTF("loginok");
                        setUser(Server.stored_data.get(username_input));
                        Server.users_DATA.put(username_input,this);
                        loginchekc = false;
                        done = false;
                    }
                }
                else if (Server.users_DATA.get(username_input).getUser().getPassword().equals(password_input)) {
                        System.out.println("login done");
                        dos.writeUTF("loginok");
                         setUser( Server.users_DATA.get(username_input).getUser());
                        loginchekc=false;
                        done=false;
                    }

                else{
                    dos.writeUTF("wrong");
                    System.out.println("wrong pass");
                    }
                }








            } else if (input.equals("signup")) {
                boolean signup = true;
                while (signup){
                    System.out.println("sign up process start");
                input = dis.readUTF();
                username_input = input;
                if (!check_input_signup(username_input)) {
                    dos.writeUTF("wrong");
                    sign_up_prcess();
                } else {
                    String folder_name = username_input;
                    File folder = new File(Server.path + "\\file-datacenter\\" + folder_name);
                    boolean ok = folder.mkdir();


                    String file_name = "user-pass.txt";
                    File file = new File(Server.path + "\\file-datacenter\\" + folder_name + "\\" + file_name);
                    file.createNewFile();


                    File file0 = new File(Server.path + "\\file-datacenter\\all-usernames.txt");
                    file0.createNewFile();
                    FileWriter fw0 = new FileWriter(file0, true);
                    fw0.write(username_input + " ");
                    fw0.close();


                    FileWriter fw = new FileWriter(file);
                    fw.write(username_input + "\n");
                    input = dis.readUTF();
                    password_input = input;
                    fw.write(password_input + "\n");
                    input = dis.readUTF();


                    String file_name2 = "name" + ".txt";
                    File file2 = new File(Server.path + "\\file-datacenter\\" + folder_name + "\\" + file_name2);
                    file2.createNewFile();
                    FileWriter fw2 = new FileWriter(file2);
                    name = input;
                    fw2.write(name + "\n");


                    String file_name3 = "bio" + ".txt";
                    File file3 = new File(Server.path + "\\file-datacenter\\" + folder_name + "\\" + file_name3);
                    file3.createNewFile();
                    FileWriter fw3 = new FileWriter(file3);
                    input = dis.readUTF();
                    bio = input;
                    fw3.write(bio + "\n");

                    fw.close();

                    Users u1 = new Users(username_input, password_input, name, bio);
                    Server.stored_data.put(username_input, u1);
                    dos.writeUTF("signupok");
                    done = false;


                    getUser().setPassword(password_input);
                    getUser().setUsername(username_input);
                    getUser().setName(name);
                    getUser().setBio(bio);
                    Server.users_DATA.put(username_input, this);


                    fw2.close();
                    fw3.close();


                    System.out.println("stored data size:" + Server.stored_data.size());
                    signup=false;
                }
            }
                }

        }
    }
}
